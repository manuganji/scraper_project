HINDU_FEED = dict(feed_url = 'http://www.thehindu.com/opinion/editorial/?service=rss', 
                  editorial_url ='www.thehindu.com/opinion/editorial/',
                  publication_name = 'The Hindu', alignment = 'floatLeft',)

TOI_FEED = dict(feed_url = 'http://timesofindia.feedsportal.com/c/33039/f/533930/index.rss', 
                editorial_url = 'http://timesofindia.indiatimes.com/home/opinion/edit-page',
                publication_name = 'Times of India', alignment = 'floatLeft',)

IE_FEED = dict(feed_url = 'http://syndication.indianexpress.com/rss/35/editorials.xml',
               editorial_url = 'http://www.indianexpress.com/section/editorial/35',
               publication_name = 'Indian Express', alignment = 'floatRight',)

PIB_FEED = dict(feed_url = 'http://pib.nic.in/newsite/rssenglish.aspx',
                editorial_url = 'http://pib.nic.in/',
                publication_name = 'Press Information Bureau', alignment = 'floatRight',)

EPW_FEED = dict(feed_url = 'http://www.epw.in/feed/editorials.xml',
                editorial_url = 'http://www.epw.in/editorials',
                publication_name = 'Economic and Political Weekly', alignment = 'floatLeft',)

DTE_FEED = dict(feed_url = 'http://www.downtoearth.org.in/rss.xml',
                editorial_url = 'http://www.downtoearth.org.in/',
                publication_name = 'Down to Earth', alignment = 'floatRight',)
