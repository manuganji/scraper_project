# standard library imports
from time import strftime

# third party imports
import feedparser # pip install feedparser

# personal imports
import feedlinks

class RSSFeedParser(object):
    
    def __init__(self, feed_params):
        self.feed_url = feed_params['feed_url']
        self.editorial_url = feed_params['editorial_url']
        self.publication_name = feed_params['publication_name']
        self.alignment = feed_params['alignment']
        print "Accessing feed from " + self.publication_name
        self.feed = feedparser.parse(self.feed_url)
        self.is_toi = 'timesofindia' in self.feed_url

    def get_links(self):
        if self.is_toi:
            return [toi_entry.id for toi_entry in self.feed.entries] 
        else:
            return [entry.links[0].href for entry in self.feed.entries]

    def get_content(self):
        if self.is_toi:

            def get_only_sentences(desc):
                # I observed that the html that toi puts in this summary 
                # always starts with this <img tag
                
                last_index = desc.find('<img')
                return desc[:last_index]

            return [dict(title = toi_entry.title,
                         summary = get_only_sentences(toi_entry.summary), 
                         link = toi_entry.id) for toi_entry in self.feed.entries]
        else:
            return [dict(title = entry.title,
                         summary = entry.summary, 
                         link = entry.links[0].href) for entry in self.feed.entries]

    def get_content_wrapped(self):
    
        html = "\n<div class = 'rssContainer "+self.alignment+"'>\n"
        html += "\t<div class = 'channelTitle'>\n\t<a href=" 
        html += "'" + self.editorial_url + "'"
        html += " target = '_blank'>"
        html += self.publication_name
        html += "\t</a>\n\t</div>\n" # channelTitle tag closes here

        for item in self.get_content():
            html += "\n\t<div class = 'feedItemOuter '>\n"
            html += "\t\t<div class = 'feedItemTitle'>\n\t\t\t<a href=" 
            html += "'" + item['link'] + "'"
            html += "target = '_blank'>"
            html += item['title']
            html += "\t\t\t</a>\n\t\t</div>\n" # feedItemTitle tag closes here
            html += "\t\t<div class = 'feedItemDesc'>"
            html += item['summary']
            html += "\n\t\t</div>\n" # feedItemDesc tag closes here
            html += "\t\t<p></p>\n\t</div>" # feedItemOuter tag closes here
        
        html += "\n</div>" # rssContainer tag closes here

        return html.encode('utf-8') # since some of them will be ascii encoded

filename = strftime('News_today_%d_%b_%y.html')
file_obj = open(filename, 'w')

hindu_obj = RSSFeedParser(feedlinks.HINDU_FEED)
toi_obj = RSSFeedParser(feedlinks.TOI_FEED)
ie_obj = RSSFeedParser(feedlinks.IE_FEED)
pib_obj = RSSFeedParser(feedlinks.PIB_FEED)
epw_obj = RSSFeedParser(feedlinks.EPW_FEED)
# dte_obj = RSSFeedParser(feedlinks.DTE_FEED)

file_obj.write(hindu_obj.get_content_wrapped())
file_obj.write("\n \n") 
file_obj.write(ie_obj.get_content_wrapped())
file_obj.write("\n\n")
file_obj.write(toi_obj.get_content_wrapped())
file_obj.write("\n\n")
file_obj.write(pib_obj.get_content_wrapped())
file_obj.write("\n\n")
file_obj.write(epw_obj.get_content_wrapped())
# file_obj.write("\n\n")
# file_obj.write(dte_obj.get_content_wrapped())
file_obj.close()
