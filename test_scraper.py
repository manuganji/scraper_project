import unittest

from scraper import RSSFeedParser

import feedlinks

class HTMLTests(unittest.TestCase):
    def setUp(self):
        
        self.hindu_object = RSSFeedParser(feedlinks.HINDU_FEED)
        self.toi_object = RSSFeedParser(feedlinks.TOI_FEED)
        self.ie_object = RSSFeedParser(feedlinks.IE_FEED)

    def human_confirm(self, content, paper_name):
        print 'The content : \n' + ',\n'.join(content)
        user_resp = raw_input('Confirm expected content from the %s ("y" for yes, "n" for no") :' %paper_name)
        return user_resp == 'y' 
        
    def test_hindu(self):
        
        hindu_links = self.hindu_object.get_links()
        self.failUnless(self.human_confirm(hindu_links, 'the hindu'))
    
    def test_toi(self):
        
        toi_links = self.toi_object.get_links() 
        self.failUnless(self.human_confirm(toi_links, 'toi'))
    
    def test_ie(self):
        
        ie_links = self.ie_object.get_links()
        self.failUnless(self.human_confirm(ie_links, 'ie'))

    def test_hindu_parsed_output(self):
        content = self.hindu_object.get_content()
        self.failUnless(
                self.human_confirm( 
                        ['***\n'.join(entry.values()) for entry in content], 
                        'the hindu')
                        )

    def test_ie_parsed_output(self):
        content = self.ie_object.get_content()
        self.failUnless(
                self.human_confirm( 
                        ['***\n'.join(entry.values()) for entry in content], 
                        'ie')
                        )

    def test_toi_parsed_output(self):
        content = self.toi_object.get_content()
        self.failUnless(
                self.human_confirm( 
                        ['***\n'.join(entry.values()) for entry in content], 
                        'toi')
                        )
